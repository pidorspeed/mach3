﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class levels : MonoBehaviour
{
    
    public static int countUnlockedLevel = 1;

    [SerializeField]
    Sprite unlockedIcon;

    [SerializeField]
    Sprite lockedIcon;

    void Start()
    {   if (PlayerPrefs.GetInt("lvlunlock") < 1)
        {
            PlayerPrefs.SetInt("lvlunlock", 1);
        }
        countUnlockedLevel = PlayerPrefs.GetInt("lvlunlock");
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i < countUnlockedLevel)
            {
                #region FirstStateBtn
                transform.GetChild(i).GetComponent<Image>().sprite = unlockedIcon;
                transform.GetChild(i).GetComponent<Button>().interactable = true;
                #endregion
            }
            else
            {
                #region SndStateBtn
                transform.GetChild(i).GetComponent<Image>().sprite = lockedIcon;
                transform.GetChild(i).GetComponent<Button>().interactable = false;
                #endregion

            }

        }




    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}









    //public void backtomenu()
    //{
    //    Application.LoadLevel(0);

    //}


