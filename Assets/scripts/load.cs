﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class load : MonoBehaviour
{
    // Start is called before the first frame update

    public void Level1()
    {
        Application.LoadLevel("level_1");   
    }

    public void Level2()
    {
        Application.LoadLevel("level_2");
    }

    public void Level3()
    {
        Application.LoadLevel("level_3");
    }

    public void LoadLevel()
    {
        Application.LoadLevel("Menu");
    }

    public void LoadLevels()
    {
        Application.LoadLevel("levels");
    }
}
