﻿
using UnityEngine;
using UnityEngine.UI;



public class level_2 : MonoBehaviour
{
    Animator anm;
    GameObject[,] map = new GameObject[col, row];
    public GameObject circl;
    public static int col = 10, row = 8;
    int k = 0;// считает кол-во кликов
    public struct point
    {
        public int x, y;
    }
    public point p, p1;
    public int color, color1;
    public GameObject guiTextLink;
    public GameObject guiTextLink1;
    int[,] mas = new int[10, 8];
    int ch = 0;
    public int score = 0;
    bool sc = false;
    float tm = 60;
    int scnumb = 10;
    float timer = 0;
    int new_obj = 0;
    int n = 4;
    float drop = 1f;
    float tdel = 0.5f;
    float tdelet = 1.5f;
    float tdelet1 = 0.0f;
    int st = 0;

    void Start()
    {
        createpole();
        //compare();

    }
    // Update is called once per frame
    void Update()
    {

        mach3horiz();
        mach3vert();
        check();
        sc_time();
        timer1();

        drop -= Time.deltaTime;
        if (drop < 0)
        {
            drop_obj();
            drop = 1f;
        }

        tdel -= Time.deltaTime;
        if (tdel < 0)
        {
            andel();

            tdel = 0.5f;
        }
        tdelet -= Time.deltaTime;
        if (tdelet < 0)
        {
            del();
            tdelet = 1.5f;
        }
        bestsc();



    }


    //метод принимающий и обраб. данные с объекта
    public void position_n_color(int Y, int X)
    {
        ObjBehaviourScript_2 cxcy = map[Y, X].GetComponent<ObjBehaviourScript_2>();

        if (k == 0)
        {
            //действия если кликнули первый раз
            p.x = cxcy.cx;
            p.y = cxcy.cy;
            color = cxcy.index;
            k++;
            anm = map[Y, X].GetComponent<Animator>();
            anm.SetBool("choise", true);
        }
        else
        {
            //действия если кликнули второй раз
            p1.x = cxcy.cx;
            p1.y = cxcy.cy;
            color1 = cxcy.index;
            if ((p.x + 1 == p1.x) || (p.x - 1 == p1.x))
            {
                if (p.y == p1.y)
                {
                    cxcy.index = color;
                    map[p.y, p.x].GetComponent<ObjBehaviourScript_2>().index = color1;
                }
            }
            if ((p.y + 1 == p1.y) || (p.y - 1 == p1.y))
            {
                if (p.x == p1.x)
                {
                    cxcy.index = color;
                    map[p.y, p.x].GetComponent<ObjBehaviourScript_2>().index = color1;
                }
            }
            k--;
            ch = 1;
            sc = true;
            map[p.y, p.x].GetComponent<Animator>().SetBool("choise", false);

        }
    }
    //метод осуществляющий заполнение пустых мест
    void drop_obj()
    {
        for (int X = 2; X < row - 2; X++)
            {
            for (int Y = 0; Y < col; Y++)
            {
                if (map[Y, X].GetComponent<ObjBehaviourScript_2>().index == 0)
                {
                    int dy = Y - 1;
                    if (dy > -1)
                    {
                        map[Y, X].GetComponent<ObjBehaviourScript_2>().index = map[dy, X].GetComponent<ObjBehaviourScript_2>().index;
                        // map[dy, X].GetComponent<Animator>().SetTrigger("del");
                        //andel();
                        //del();
                        map[dy, X].GetComponent<ObjBehaviourScript_2>().index = 0;
                        map[Y, X].GetComponent<Animator>().SetTrigger("gen");


                    }
                    else
                    {
                        if (new_obj > 100)
                        {
                            n++;
                            new_obj = 0;
                        }
                        if (n < 8)
                        {
                            map[Y, X].GetComponent<ObjBehaviourScript_2>().index = Random.Range(1, n);
                            map[Y, X].GetComponent<Animator>().SetTrigger("gen");

                        }
                        else
                        {

                            if (PlayerPrefs.GetInt("lvlunlock") < 3)
                            {
                                PlayerPrefs.SetInt("lvlunlock", 3);
                            }

                            Application.LoadLevel(1);

                        }

                    }

                }
            }

        }
        for (int X = 0; X < 2; X++)
        {
            for (int Y = 2; Y < col-2; Y++)
            {
                if (map[Y, X].GetComponent<ObjBehaviourScript_2>().index == 0)
                {
                    int dy = Y - 1;
                    if (dy > 1)
                    {
                        map[Y, X].GetComponent<ObjBehaviourScript_2>().index = map[dy, X].GetComponent<ObjBehaviourScript_2>().index;
                        // map[dy, X].GetComponent<Animator>().SetTrigger("del");
                        //andel();
                        //del();
                        map[dy, X].GetComponent<ObjBehaviourScript_2>().index = 0;
                        map[Y, X].GetComponent<Animator>().SetTrigger("gen");


                    }
                    else
                    {
                        if (new_obj > 100)
                        {
                            n++;
                            new_obj = 0;
                        }
                        if (n < 8)
                        {
                            map[Y, X].GetComponent<ObjBehaviourScript_2>().index = Random.Range(1, n);
                            map[Y, X].GetComponent<Animator>().SetTrigger("gen");

                        }
                        else
                        {
                            Application.LoadLevel("Menu");

                        }

                    }

                }
            }

        }
        for (int X = row - 1; X > 5; X--)
        {
            for (int Y = 2; Y < col - 2; Y++)
            {
                if (map[Y, X].GetComponent<ObjBehaviourScript_2>().index == 0)
                {
                    int dy = Y - 1;
                    if (dy > 1)
                    {
                        map[Y, X].GetComponent<ObjBehaviourScript_2>().index = map[dy, X].GetComponent<ObjBehaviourScript_2>().index;
                       //map[dy, X].GetComponent<Animator>().SetTrigger("del");
                        
                        map[dy, X].GetComponent<ObjBehaviourScript_2>().index = 0;
                        map[Y, X].GetComponent<Animator>().SetTrigger("gen");


                    }
                    else
                    {
                        if (new_obj > 100)
                        {
                            n++;
                            new_obj = 0;
                        }
                        if (n < 8)
                        {
                            map[Y, X].GetComponent<ObjBehaviourScript_2>().index = Random.Range(1, n);
                            map[Y, X].GetComponent<Animator>().SetTrigger("gen");

                        }
                        else
                        {
                            if (PlayerPrefs.GetInt("lvlunlock") < 3)
                            {
                                PlayerPrefs.SetInt("lvlunlock", 3);
                            }
                            Application.LoadLevel(1);

                        }

                    }

                }
            }

        }

    }
    void mach3horiz()
    {
        for (int Y = 0; Y < col; Y++)
        {
            int numbind = 0;//номер индекса
            int coincid = 0;// кол-во совпадений
            for (int X = 0; X < row; X++)
            {
                if (X == 0)
                {
                    numbind = map[Y, X].GetComponent<ObjBehaviourScript_2>().index;
                }

                if (map[Y, X].GetComponent<ObjBehaviourScript_2>().index == numbind)
                {
                    coincid++;
                }
                else
                {
                    if (coincid > 2)
                    {
                        for (int i = 0; i <= coincid - 1; i++)
                        {
                            mas[Y, X - coincid + i] = map[Y, X - coincid + i].GetComponent<ObjBehaviourScript_2>().index;
                        }
                    }
                    numbind = map[Y, X].GetComponent<ObjBehaviourScript_2>().index;
                    coincid = 1;
                }
                if ((X == row - 1) && (coincid > 2))
                {
                    for (int i = 1; i < coincid + 1; i++)
                    {
                        mas[Y, X - coincid + i] = map[Y, X - coincid + i].GetComponent<ObjBehaviourScript_2>().index;
                    }
                }


            }

        }
    }
    void mach3vert()
    {
        for (int X = 0; X < row; X++)
        {
            int numbind = 0;//номер индекса
            int coincid = 0;// кол-во совпадений
            for (int Y = 0; Y < col; Y++)
            {
                if (Y == 0) { numbind = map[Y, X].GetComponent<ObjBehaviourScript_2>().index; }
                if (map[Y, X].GetComponent<ObjBehaviourScript_2>().index == numbind)
                {
                    coincid++;
                }
                else
                {
                    if (coincid > 2)
                    {
                        for (int i = 0; i <= coincid - 1; i++)
                        {
                            mas[Y - coincid + i, X] = map[Y - coincid + i, X].GetComponent<ObjBehaviourScript_2>().index;
                        }
                    }
                    numbind = map[Y, X].GetComponent<ObjBehaviourScript_2>().index;
                    coincid = 1;
                }
                if ((Y == col - 1) && (coincid > 2))
                {
                    for (int i = 1; i < coincid + 1; i++)
                    {
                        mas[Y - coincid + i, X] = map[Y - coincid + i, X].GetComponent<ObjBehaviourScript_2>().index;
                    }
                }


            }

        }
    }
    // для удаленя "уголком"
    void andel()
    {
        for (int Y = 0; Y < col; Y++)
        {
            for (int X = 0; X < row; X++)
            {
                if (mas[Y, X] > 0)
                {

                    map[Y, X].GetComponent<Animator>().SetTrigger("del");
                }


            }
        }

    }
    void check()
    {

        if ((mas[p1.y, p1.x] == 0) && (mas[p.y, p.x] == 0))
        {
            if (ch == 1)
            {
                map[p.y, p.x].GetComponent<ObjBehaviourScript_2>().index = color;
                map[p1.y, p1.x].GetComponent<ObjBehaviourScript_2>().index = color1;
                ch = 0;

            }

        }

    }
    void sc_time()
    {
        tm -= (Time.deltaTime);
        if (tm < 0)
        {
            tm = 60;
            if (scnumb > 2)
            {
                scnumb--;
            }
        }
    }
    void timer1()
    {
        timer += (Time.deltaTime);

        guiTextLink1.GetComponent<Text>().text = "Timer: " + ((int)(timer) + score);

    }
    void bestsc()
    {
        if (PlayerPrefs.GetInt("bscore") < score)
            PlayerPrefs.SetInt("bscore", score);
    }
    void del()
    {
        for (int Y = 0; Y < col; Y++)
        {
            for (int X = 0; X < row; X++)
            {
                if (mas[Y, X] > 0)
                {


                    map[Y, X].GetComponent<ObjBehaviourScript_2>().index = 0;
                    mas[Y, X] = 0;
                    ch = 0;


                    if (sc == true)
                    {
                        score += scnumb;
                        new_obj += scnumb;
                        guiTextLink.GetComponent<Text>().text = "Score: " + score;
                    }
                }

            }
        }

    }
    void createpole()
    {

        Vector2 poze = new Vector2(-2.4f,2.7f);
        for (int Y = 0; Y < col; Y++)
        {
            for (int X = 0; X < row; X++)
            {
                map[Y, X] = Instantiate(circl, poze, Quaternion.identity) as GameObject;
                ObjBehaviourScript_2 cxcy = map[Y, X].GetComponent<ObjBehaviourScript_2>();
                cxcy.cy = Y;
                cxcy.cx = X;
                cxcy.index = 0;// Random.Range(1, 4);

                cxcy.mymaingame = this.gameObject;
                poze.x += 0.67f;
            }
            poze.x = -2.4f;
            poze.y -= 0.67f;
        }
        for (int Y = 0; Y < 2; Y++)
        {
            for (int X = 2; X < row-2; X++)
            {
                 map[Y, X].GetComponent<ObjBehaviourScript_2>().index=Random.Range(1, 4);
            }
           
        }

        for (int Y = col-1; Y > 7; Y--)
        {
            for (int X = 2; X < row - 2; X++)
            {
                map[Y, X].GetComponent<ObjBehaviourScript_2>().index = Random.Range(1, 4);
            }

        }
        for (int Y = 2; Y < 8; Y++)
        {
            for (int X = 0; X < row; X++)
            {
                map[Y, X].GetComponent<ObjBehaviourScript_2>().index = Random.Range(1, 4);
            }

        }

        for (int X = 0; X < row; X++)
        {
            for (int Y = 0; Y < col; Y++)
            {

                mach3horiz();
                mach3vert();
                del();
                drop_obj();

            }
        }
    }
  
}