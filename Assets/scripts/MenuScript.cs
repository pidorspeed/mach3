﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
    public GameObject buttonsMenu;
    public GameObject buttonsExit;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowExitButtons()
    {
        
        buttonsMenu.SetActive(false);
        buttonsExit.SetActive(true);
    }

    public void BackInMenu()
    {
        buttonsMenu.SetActive(true);
        buttonsExit.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();

    }

    public void NewGameStart()
    {
        Application.LoadLevel(1);
    }

   
}
